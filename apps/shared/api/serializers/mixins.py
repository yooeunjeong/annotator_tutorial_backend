import copy

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework.settings import api_settings


class SerializerCleanMixin:
    validation_errors = []

    def is_valid(self, raise_exception=False):
        is_valid = super().is_valid(raise_exception=False)

        self.clean()

        if self._errors and raise_exception:
            raise serializers.ValidationError(self.errors)
        return is_valid

    def clean(self):
        raise NotImplementedError

    def add_error(self, message, field=api_settings.NON_FIELD_ERRORS_KEY):
        try:
            self._errors[field].append(message)
        except KeyError:
            self._errors[field] = [message]


class ModelCleanMixin:

    def is_valid(self, raise_exception=False):
        is_valid = super().is_valid(raise_exception=False)
        model_class = self.Meta.model
        cleaned_data = self.get_cleaned_data(copy.deepcopy(self.validated_data))

        try:
            model_class(**cleaned_data).clean()
        except ValidationError as exc:
            self._errors.update(serializers.as_serializer_error(exc))

        if self._errors and raise_exception:
            raise serializers.ValidationError(self.errors)
        return is_valid

    def get_cleaned_data(self, attrs):
        return attrs


class DynamicFieldsMixin:

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super().__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class FilteredQuerySetMixin(SerializerCleanMixin):
    filtered_queryset = None

    def clean(self):
        view = self.context['view']
        queryset = view.filter_queryset(view.get_queryset())
        if not queryset.exists():
            self.add_error(_('처리할 수 있는 객체가 없습니다.'))
        self.filtered_queryset = queryset

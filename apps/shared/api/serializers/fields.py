from rest_framework import serializers


class AbsoluteURLField(serializers.Field):

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        request = self.context.get('request', None)
        if request is not None and value:
            return request.build_absolute_uri(value)
        return value


class CurrentWorkspaceDefault:
    requires_context = True

    def __call__(self, serializer_field):
        return serializer_field.context['request'].membership.workspace

    def __repr__(self):
        return '%s()' % self.__class__.__name__

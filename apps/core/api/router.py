from importlib import import_module

from rest_framework.routers import DefaultRouter


class ExtendedRouter(DefaultRouter):
    def extend(self, urlconf):
        urlconf_module = import_module(urlconf)
        router = getattr(urlconf_module, 'router', None)
        if router:
            self.registry.extend(router.registry)

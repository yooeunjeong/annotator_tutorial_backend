from django.contrib.contenttypes.models import ContentType
from generic_relations.relations import GenericRelatedField

from apps.board.models import Post, Comment, FAQ, Media
from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer
from drf_extra_fields.fields import Base64ImageField


class MediaSerializer(FlexFieldsModelSerializer):
    file = Base64ImageField(required=False)

    class Meta:
        model = Media
        fields = ['file']


class CommentSerializer(FlexFieldsModelSerializer):
    files = MediaSerializer(many=True, required=False)

    class Meta:
        model = Comment
        fields = ['post', 'user', 'content', 'created', 'files']
        read_only_fields = ['user', 'created', 'files']

    def create(self, validated_data):
        files = validated_data.pop('files')
        instance = super().create(validated_data)
        content_type = ContentType.objects.get_for_model(instance)

        Media.objects.filter(object_id=instance.id, content_type=content_type).delete()
        for file in files:
            Media.objects.create(object_id=instance.id, content_type=content_type, file=file['file'])

        return instance


class CommentUpdateSerializer(FlexFieldsModelSerializer):
    files = MediaSerializer(many=True, required=False)

    class Meta:
        model = Comment
        fields = ['content', 'files']

    def update(self, instance, validated_data):
        files_data = validated_data.pop('files')
        instance = super().update(instance, validated_data)
        content_type = ContentType.objects.get_for_model(instance)

        Media.objects.filter(object_id=instance.id, content_type=content_type).delete()
        for file_data in files_data:
            Media.objects.create(object_id=instance.id, content_type=content_type, file=file_data['file'])

        return instance


class PostSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)
    files = MediaSerializer(many=True, required=False)

    class Meta:
        model = Post
        fields = ['id', 'board', 'title', 'content', 'user', 'comments', 'files', 'site', 'count_read']
        read_only_fields = ['id', 'user', 'comments', 'count_read']

    def validate(self, attrs):
        board = attrs['board']

        if board in Post.admin_board_list:
            raise serializers.ValidationError({'error_message': '관리자만 접근할 수 있습니다.'})

        return attrs

    # files upload
    def save(self, **kwargs):
        files_data = self.validated_data.pop('files')

        instance = super().save(**kwargs)
        content_type = ContentType.objects.get_for_model(instance)

        Media.objects.filter(object_id=instance.id, content_type=content_type).delete()
        for file_data in files_data:
            Media.objects.create(object_id=instance.id, content_type=content_type, file=file_data['file'])

        return self.instance


class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = ['category', 'question', 'answer']

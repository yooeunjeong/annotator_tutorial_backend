from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.db import models
from django_ckeditor_5.fields import CKEditor5Field

from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from functools import reduce
from operator import or_


class PostQuerySet(models.QuerySet):
    def to_admin(self, user):
        query = reduce(or_, (Q(board=admin_board) for admin_board in Post.admin_board_list))
        return self.filter(query)

    def to_labeler(self, user):
        return self.exclude(board__in=Post.admin_board_list).filter(user=user)

    def readable(self, user):
        return self.filter(Q(board=Post.Board.NOTICE) | Q(user=user)).distinct()


class Post(models.Model):
    class Board(models.TextChoices):
        NOTICE = 'notice', _('공지사항')
        INQUIRY = 'inquiry', _('문의하기')

    admin_board_list = [Board.NOTICE]
    labeler_board_list = [Board.INQUIRY]

    board = models.CharField(_('게시판'), max_length=20, choices=Board.choices)
    title = models.CharField(_('제목'), max_length=100)
    content = CKEditor5Field(_('내용'))

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='posts',
        verbose_name='회원', on_delete=models.CASCADE
    )
    count_read = models.IntegerField(_('조회수'), default=0)
    content_type = models.ForeignKey(
        'contenttypes.ContentType', blank=True, null=True,
        on_delete=models.CASCADE, verbose_name='타입'
    )

    objects = PostQuerySet.as_manager()
    object_id = models.PositiveIntegerField(_('오브젝트 고유번호'), blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    site = models.ForeignKey(Site, verbose_name='사이트', on_delete=models.PROTECT)
    created = models.DateTimeField(_('생성일'), auto_now_add=True)
    modified = models.DateTimeField(_('수정일'), auto_now=True)

    class Meta:
        verbose_name = '게시글'
        verbose_name_plural = '게시글'


class Comment(models.Model):
    post = models.ForeignKey(
        'Post', related_name='comments',
        verbose_name='게시글', on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='comments',
        verbose_name='작성자', on_delete=models.DO_NOTHING
    )

    content = models.TextField(_('내용'))
    created = models.DateTimeField(_('생성일'), auto_now_add=True)
    modified = models.DateTimeField(_('수정일'), auto_now=True)

    class Meta:
        verbose_name = '댓글'
        verbose_name_plural = '댓글'


class FAQ(models.Model):
    class Category(models.TextChoices):
        RECRUITMENT = 'recruitment', _('채용/회원')
        TASK = 'task', _('작업')
        PAYMENT = 'payment', _('급여')
        ETC = 'etc', _('기타')

    category = models.CharField(_('카테고리'), max_length=20, choices=Category.choices)
    question = models.TextField(_('질문'))
    answer = models.TextField(_('답변'))
    order = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(_('생성일'), auto_now_add=True)
    modified = models.DateTimeField(_('수정일'), auto_now=True)

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'
        ordering = ['-order']


class Media(models.Model):
    file = models.FileField(_('첨부파일'), blank=True)

    content_type = models.ForeignKey(
        'contenttypes.ContentType',
        on_delete=models.CASCADE, verbose_name='타입'
    )
    object_id = models.PositiveIntegerField(_('오브젝트 고유번호'))
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(_('생성일'), auto_now_add=True)

    class Meta:
        verbose_name = '미디어 파일'
        verbose_name_plural = '미디어 파일'

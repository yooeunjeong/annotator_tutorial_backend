from django.db.models import F
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, mixins
from rest_framework.response import Response

from apps.board.filters import PostFilter, FAQFilter

from apps.board.models import Post, FAQ, Comment, Media
from apps.board.serializers import PostSerializer, FAQSerializer, CommentSerializer, \
    CommentUpdateSerializer
from apps.shared.api.viewsets.mixins import DynamicSerializerMixin


class PostLabelerViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin,
                         mixins.UpdateModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Post.objects.all()
    filter_class = PostFilter
    search_fields = ['title', 'content']

    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.readable(self.request.user)

    # 조회수 증가
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        Post.objects.filter(pk=instance.id).update(count_read=F('count_read') + 1)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class FAQViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = FAQ.objects.all().order_by('order')
    serializer_class = FAQSerializer
    filter_class = FAQFilter
    search_fields = ['question']


class CommentViewSet(DynamicSerializerMixin, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    serializer_classes = {
        'update': CommentUpdateSerializer
    }

    def get_queryset(self):
        return Comment.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

import django_filters
from .models import Post, FAQ


class PostFilter(django_filters.FilterSet):
    class Meta:
        model = Post
        fields = ['board']


class FAQFilter(django_filters.FilterSet):
    class Meta:
        model = FAQ
        fields = ['category']

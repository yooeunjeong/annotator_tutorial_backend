from django import forms

from apps.board.models import Post
from django.utils.translation import gettext_lazy as _


class PostForm(forms.ModelForm):
    user = None
    project = None

    class Meta:
        model = Post
        fields = ['board', 'title', 'content', 'file']
        widgets = {
            'content': forms.Textarea()
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.project = kwargs.pop('project', None)

        super().__init__(*args, **kwargs)
        self.fields['content'].widget = forms.Textarea()
        self.fields['board'].choices = [
            ('inquiry', _('문의하기'))
        ]

    def save(self, commit=True):
        instance = super().save(commit=False)
        if commit:
            instance.user = self.user
            if self.project:
                instance.content_object = self.project
            instance.save()
        return instance


class CommentForm(forms.ModelForm):
    class Meta:
        fields = ['content', 'file']
        widgets = {
            'content': forms.Textarea()
        }


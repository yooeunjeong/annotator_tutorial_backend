from rest_framework.routers import DefaultRouter
from .views import PostLabelerViewSet, FAQViewSet, CommentViewSet

router = DefaultRouter()

router.register('posts', PostLabelerViewSet)
router.register('faq', FAQViewSet)
router.register('comments', CommentViewSet)

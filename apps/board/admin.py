from django.contrib import admin

# Register your models here.
from apps.board.models import Post, Comment, FAQ, Media


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = [
        'board', 'title', 'user', 'count_read', 'created'
    ]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['post', 'user', 'created']


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ['category', 'question', 'answer', 'order']


@admin.register(Media)
class MediaAdmin(admin.ModelAdmin):
    list_display = ['file', 'content_type', 'object_id', 'content_object']
